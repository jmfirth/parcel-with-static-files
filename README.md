# parcel-with-static-files

This is a demo of using parcel when there is a need to reference static files.

## Getting Started

```sh
# install dependencies
yarn install

# start development server
yarn start
```
